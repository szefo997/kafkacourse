package com.bitbucket.kafka.tut1;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.errors.WakeupException;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

import static com.bitbucket.kafka.tut1.ProducerDemo.TOPIC;

public final class ConsumerDemo {

    private final Logger log = LoggerFactory.getLogger(ConsumerDemo.class);

    private ConsumerDemo() {
    }

    public static void main(String[] args) {
        new ConsumerDemo().run();
    }

    private void run() {
        // latch for dealing with multiple threads
        final CountDownLatch latch = new CountDownLatch(1);

        // create consumer runnable
        final CustomerThread runnable = new CustomerThread(latch,
                "127.0.0.1:9092",
                TOPIC,
                "my-sixth-application");

        // start thread
        final Thread thread = new Thread(runnable);
        thread.start();

        // add a shutdown hook
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            log.info("Caught shutdown hook");
            runnable.shutdown();
            try {
                latch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }));

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
            log.error("Application is interrupted message: {}", e.getMessage());
        } finally {
            log.info("Application is closing");
        }
    }

    class CustomerThread implements Runnable {

        private final CountDownLatch latch;
        private final KafkaConsumer<String, String> consumer;

        CustomerThread(CountDownLatch latch, String bootstrapServers, String topic, String groupId) {
            this.latch = latch;
            final String name = StringDeserializer.class.getName();
            final Properties properties = new Properties();

            properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
            properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, name);
            properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, name);
            properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, groupId);
            properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

            this.consumer = new KafkaConsumer<>(properties);
            consumer.subscribe(Collections.singletonList(topic));
        }

        @Override
        public void run() {
            try {
                // poll new data
                while (true) {
                    for (final ConsumerRecord<String, String> r : consumer.poll(Duration.ofMillis(100))) {
                        log.info("Key : {}, Value: {}, Partition: {}, Offset: {}", r.key(), r.value(), r.partition(), r.offset());
                    }
                }
            } catch (WakeupException ex) {
                log.info("Received shutdown");
            } finally {
                consumer.close();
                latch.countDown();
            }
        }

        void shutdown() {
            // interrupt
            // it will throw exception
            consumer.wakeup();
        }

    }


}
