package com.bitbucket.kafka.tut1;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;
import java.util.concurrent.ExecutionException;

public final class ProducerDemo {

    final static String TOPIC = "first_topic";

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        final Logger log = LoggerFactory.getLogger(ProducerDemo.class);
        final String bootstrapServers = "127.0.0.1:9092";
        // create Producer properties
        final Properties properties = new Properties();
        final String name = StringSerializer.class.getName();

        properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, name);
        properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, name);

        // create producer
        final KafkaProducer<String, String> producer = new KafkaProducer<>(properties);

        for (int i = 0; i < 10; i++) {
            //create producer record
            final String value = "hello world : " + i;
            final String key = "id_" + i;

            log.info("Key: {} ", key);

            final ProducerRecord<String, String> record = new ProducerRecord<>(TOPIC, key, value);
            // send data asynchronous
            producer.send(record, (metadata, exception) -> {
                if (exception == null) {
                    log.info("Received new metadata TOPIC:  {}, partition: {}, offset: {}, timestamp: {} ",
                            metadata.topic(),
                            metadata.partition(),
                            metadata.offset(),
                            metadata.timestamp());
                } else {
                    log.error(exception.getMessage());
                }
            }).get(); // block send to to make it synchronous - don't do this in production
        }

        producer.flush();
        producer.close();
    }

}
